* Script sample

This script simply tries to emulate the behavior of a controller that
monitors the levels of certain poisonous gases. When the operator
exceeds certain time and concentration thresholds, a danger signal is
triggered.

** Some sample use

#+CAPTION: Beta distribution
[[./doc/beta-distribution.png]]

#+CAPTION: Lectures above 0.25
[[./doc/second-threshold.png]]

#+CAPTION: Plot graph
[[./doc/lectures.png]]

** Some web services used

#+CAPTION: Email notification
[[./doc/email-notification.png]]

#+CAPTION: Mailgun dashboard
[[./doc/mailgun-dashboard.png]]

#+CAPTION: no-ip dashboard
[[./doc/no-ip-dashboard.png]]
