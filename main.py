import time                                    # sleep every interval
import datetime                                # get actual timestamp
import random                                  # generate lectures
import smtplib                                 # email through SMTP
import csv                                     # export data to csv
import matplotlib.pyplot as plt                # draw graph
from matplotlib.dates import DateFormatter     # format dates in graph
from email.mime.multipart import MIMEMultipart # email components
from email.mime.image import MIMEImage         # email components
from email.mime.text import MIMEText           # email components

################################################################################
# GLOBAL VARIABLES
################################################################################

# Store successive lectures as history
daily_values = [[],[]]
# Store last lectures to check some values
last_values = []

################################################################################
# EMAIL MACHINERY
################################################################################

def send_smtp_message(msg_txt):
    """Send message through Mailgun, with MSG_TXT content."""
    # Construct message object
    msg = MIMEMultipart() #message.EmailMessage()
    msg['Subject'] = "DANGER from your armband !!!!"
    msg['From'] = "EPSG5 Armband <postmaster@sandbox98266d755e7140a4a841a2575fc6de96.mailgun.org>"
    msg['To'] = "EPSG5 <eps.g5.test@gmail.com>"

    # Add content from parameter
    msg.preamble = msg_txt
    msg.attach(MIMEText(msg_txt))

    # Add graph as attachment
    with open("lectures.png", "rb") as f:
        att = MIMEImage(f.read())#, Name="The graph"))
        att.add_header("Content-Disposition", "attachment", filename="lectures.png")
        msg.attach(att)

    # Add data as attachment
    with open("lectures.csv", "r") as f:
        att = MIMEText(f.read())#, Name="The data"))
        att.add_header("Content-Disposition", "attachment", filename="lectures.csv")
        msg.attach(att)

    # Send email
    with smtplib.SMTP('smtp.mailgun.org', 587) as s:
        s.login("postmaster@sandbox98266d755e7140a4a841a2575fc6de96.mailgun.org", "d5fb6c2f3ed38864c078111de00f47bd-07ec2ba2-49a77867")
        s.sendmail(msg["From"], msg["To"], msg.as_string())
        s.quit

################################################################################
# DRAW GRAPH
################################################################################

def draw_graph():
    """Plot a simple graph with the lectures."""
    # Construct plotting objects
    fig = plt.figure(figsize=(16,6))
    ax = fig.subplots()

    # Horizontal limits
    plt.axhline(y=0.3, color="red", linestyle="dashed")
    plt.axhline(y=0.25, color="blue", linestyle="dashed")
    plt.axhline(y=0.1, color="green", linestyle="dashed")

    # Path with the lectures
    ax.plot(daily_values[0], daily_values[1], linewidth=3, marker="o")

    # Format some niceties
    ax.set_ylim(ymin=0)
    ax.set_facecolor("seashell")
    ax.xaxis.set_major_formatter(DateFormatter("%Y-%m-%d %H:%M"))
    plt.setp(ax.get_xticklabels(), rotation=45, horizontalalignment="right")
    ax.set_xlabel("time (UTC)")
    ax.set_ylabel("CLO (ppm)")
    ax.grid(True, linestyle="dotted")

    # Draw 
    plt.savefig("lectures.png", metadata={"Title": "Graph"}, bbox_inches="tight")
    #plt.show()

################################################################################
# CSV STORE
################################################################################

def export_csv():
    """Export values as CSV file."""
    fields = ["Timestamp", "Value"]
    values = list(zip(daily_values[0], daily_values[1]))
    with open("lectures.csv", "w") as f:
        write = csv.writer(f)
        write.writerow(fields)
        write.writerows(values)

################################################################################
# DANGER SIGNAL
################################################################################

def signal_message(msg, val):
    """Do the warning. MSG is the textual message, and VAL the data values."""
    # Print on console
    print(msg, val)
    print("-- LIGHT ON --")
    print("-- BUZZER ON --")
    print("Daily values:", daily_values)

    # Create graph
    draw_graph()

    # Export values
    export_csv()

    # Send email
    send_smtp_message(msg + " " + str(val))
    print("Mail sent")

################################################################################
# PROCESS SENSOR LECTURES
################################################################################

# Infinite loop until the DANGER signal is sent
while True:
    # Interval between lectures
    time.sleep(0.1) # Should be 60 seconds

    # Generate a lecture. There are different statistical distributions to use,
    # uncomment the chosen one:
    value = random.betavariate(0.5, 5) # Lectures in a small last_values beta distribution
    #value = random.uniform(0.25, 0.3) # Lectures between 0.25 and 0.3
    #value = random.uniform(0.23, 0.3) # Lectures between 0.24 and 0.3
    #value = random.uniform(0.1, 0.3) # Lectures between 0.1 and 0.3
    #value = random.uniform(0.09, 0.3) # Lectures between 0.09 and 0.3
    # Round value to 2 decimals
    value = round(value, 2)
    daily_values[0].append(datetime.datetime.now())
    daily_values[1].append(value)

    # One lecture above 0.3 is signaled as DANGER
    if value > 0.3:
        signal_message("DANGER, value exceeds 0.3:", value)
        break
    # Any lecture above 0.1 is stored in the 'last_values' list, then if the last
    # 15 lectures are above 0.25 the DANGER is signaled. If the list exceeds
    # 480 lectures (8 hours * 60 minutes) the DANGER is signaled.
    elif value >= 0.1:
        last_values.append(value)
        print("IN DANGER ZONE:", last_values)
        n = len(last_values)
        if n >= 15 and min(last_values[-15:]) >= 0.25:
            signal_message("DANGER, more than 15 lectures above 0,25:", last_values)
            break
        if n > (8 * 60):
            signal_message("DANGER, more than 480 lectures above 0,1:", last_values)
            break
    # Finally, any lecture below 0.1 resets the 'last_values' list
    else:
        last_values.clear()
        print(value)
